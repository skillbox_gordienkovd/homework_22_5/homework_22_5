using Homework.Domain;
using MediatR;
using Homework.Dto;
using System.Security.Claims;

namespace Homework.Application.Commands
{
    public class AccountGetAuthenticatedQuery : IRequest<string>
    {
        public ClaimsPrincipal User { get; set; }
    }
}
