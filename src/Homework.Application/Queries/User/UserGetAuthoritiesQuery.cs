using MediatR;
using System.Collections.Generic;

namespace Homework.Application.Queries
{
    public class UserGetAuthoritiesQuery : IRequest<IEnumerable<string>>
    {
    }
}
