using Homework.Dto;
using MediatR;

namespace Homework.Application.Queries
{
    public class UserGetQuery : IRequest<UserDto>
    {
        public string Login { get; set; }
    }
}
