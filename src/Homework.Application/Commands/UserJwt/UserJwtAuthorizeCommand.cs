using MediatR;
using Homework.Dto;
using System.Security.Principal;

namespace Homework.Application.Commands
{
    public class UserJwtAuthorizeCommand : IRequest<IPrincipal>
    {
        public LoginDto LoginDto { get; set; }
    }
}
