using Homework.Domain;
using MediatR;
using Homework.Dto;

namespace Homework.Application.Commands
{
    public class AccountCreateCommand : IRequest<User>
    {
        public ManagedUserDto ManagedUserDto { get; set; }
    }
}
