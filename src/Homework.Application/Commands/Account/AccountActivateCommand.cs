using Homework.Domain;
using MediatR;

namespace Homework.Application.Commands
{
    public class AccountActivateCommand : IRequest<User>
    {
        public string Key { get; set; }
    }
}
