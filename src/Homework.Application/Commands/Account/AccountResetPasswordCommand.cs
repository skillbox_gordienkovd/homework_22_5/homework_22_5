using MediatR;

namespace Homework.Application.Commands
{
    public class AccountResetPasswordCommand : IRequest<Unit>
    {
        public string Mail { get; set; }
    }
}
