using Homework.Domain;
using MediatR;
using Homework.Dto;

namespace Homework.Application.Commands
{
    public class AccountResetPasswordFinishCommand : IRequest<User>
    {
        public KeyAndPasswordDto KeyAndPasswordDto { get; set; }
    }
}
