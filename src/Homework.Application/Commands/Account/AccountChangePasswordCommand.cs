using MediatR;
using Homework.Dto;

namespace Homework.Application.Commands
{
    public class AccountChangePasswordCommand : IRequest<Unit>
    {
        public PasswordChangeDto PasswordChangeDto { get; set; }
    }
}
