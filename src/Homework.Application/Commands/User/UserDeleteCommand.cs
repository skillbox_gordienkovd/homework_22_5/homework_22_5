using Homework.Domain;
using MediatR;

namespace Homework.Application.Commands
{
    public class UserDeleteCommand : IRequest<Unit>
    {
        public string Login { get; set; }
    }
}
