using Homework.Domain;
using MediatR;
using Homework.Dto;

namespace Homework.Application.Commands
{
    public class UserUpdateCommand : IRequest<User>
    {
        public UserDto UserDto { get; set; }
    }
}
