using Homework.Domain;
using MediatR;
using Homework.Dto;

namespace Homework.Application.Commands
{
    public class UserCreateCommand : IRequest<User>
    {
        public UserDto UserDto { get; set; }
    }
}
